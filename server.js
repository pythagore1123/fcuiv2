'use strict'
const http = require("http")
const express = require("express")
const path = require("path")
const BodyParser = require("body-parser")
var fs = require('fs')
const app = express()
var server = http.createServer(app).listen(8080,() => {
	console.log("Dkm dc roi")
})

app.use(BodyParser.json())
app.use(BodyParser.urlencoded({ extended: true }))
app.engine('html', require('ejs').renderFile)
app.set('view engine', 'html')

app.use((req, res, next) => {
	res.json = (obj) => {
 		res.setHeader('Content-Type', 'application/json')
 		res.end(JSON.stringify(obj))
	}
	next()
})

const Users = [
	{username: 'a', password: 'a'}
]

const profile = { "showOnRanking":true,"id":686,"username":"pythagore1123","name":"Lê Đình Hải","schoolName":"THPT Chuyên Lê Quý Đôn","yob":2001,"createdAt":"2018-09-09T10:33:35.478Z","updatedAt":"2018-10-21T05:22:14.466Z","color":0,"rating":0,"avatar":"./static/img/anonymous.jpg","role":"admin","contests":[{"id":984,"uid":686,"cid":43,"createdAt":"2018-09-09T10:38:04.660Z","updatedAt":"2018-09-09T10:38:04.660Z"},{"id":1726,"uid":686,"cid":50,"createdAt":"2018-10-23T14:44:04.233Z","updatedAt":"2018-10-23T14:44:04.233Z"},{"id":2109,"uid":686,"cid":57,"createdAt":"2018-11-04T14:49:44.397Z","updatedAt":"2018-11-04T14:49:44.397Z"},{"id":2469,"uid":686,"cid":60,"createdAt":"2018-11-21T13:06:48.551Z","updatedAt":"2018-11-21T13:06:48.551Z"}],"logsUpdateRating":[]}

// app.use('/public', express.static(path.resolve(__dirname, './public')))
app.use('/dist', express.static(path.resolve(__dirname, './dist')))

app.get('/api/auth', (req, res) => {
	let _res = profile
	_res.logined = true
	res.status(200).send(_res)
})

app.post("/api/auth", (req, res) => {
	const auth = req.body
	if(Users.find(u => (u.username == auth.username && u.password == auth.password)) != null){
		res.status(200).send({logined: true})
		return;
	}
	res.status(200).send({err: "user doesn't exist or password isn't correct."})
})

app.put("/api/auth", (req, res) => {
	if(req.body.name == 'err'){
		res.status(200).send({ err: 'something' })
	}
	else{
		res.status(200).send({ success: 'true' })
	}
})

app.get("/api/posts", async (req, res) => {
	if(req.query.postid != undefined){
		const _res = await fs.readFileSync(path.resolve(__dirname, './dist/static/db/posts.json'), 'utf8')
		res.status(200).send([JSON.parse(_res)[6]])
	}
	else{
		const _res = await fs.readFileSync(path.resolve(__dirname, './dist/static/db/posts.json'), 'utf8')
		res.status(200).send(JSON.parse(_res))
	}
})

app.get("/api/contest", async (req, res) => {
	const _res = await fs.readFileSync(path.resolve(__dirname, './dist/static/db/contest.json'), 'utf8')
	res.status(200).send(JSON.parse(_res))
})

app.post("/api/contest/register", async(req, res) => {
	const _res = await fs.readFileSync(path.resolve(__dirname, './dist/static/db/hasreg.json'), 'utf-8')
	res.status(200).send(JSON.parse(_res))
})

app.post('/api/singleContest', async(req, res) => {
	let _res = await fs.readFileSync(path.resolve(__dirname, './dist/static/db/contest.json'), 'utf-8')
	_res = JSON.parse(_res)[0]
	_res.code = 1
	_res.IsDuring = true
	_res.err = false
	res.status(200).send(_res)
})

app.get('/api/users', async (req, res) => {
	if(req.query.username != undefined){
		const _res = profile;
		res.status(200).send(_res)
	}
	else{
		const _res = await fs.readFileSync(path.resolve(__dirname, './dist/static/db/user.json'), 'utf8')
		res.status(200).send(JSON.parse(_res))
	}
	
})



app.get("/*",(req, res) => {
	res.render(path.resolve(__dirname,'./dist/index'), {
		path: 'ahihi'
	})
})
