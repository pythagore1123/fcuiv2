import React, { Component } from 'react'
import { FreeContest as FC, history } from '../js/action'
import Navbar from '../components/navbar'
import Init from '../js/init'


class App extends Component{

    constructor(props){
        super(props)

        this.state = {
            users: null,

            rankLoading: true
        }
    }

    componentDidMount(){
        document.title = 'Ranking'

        FC.getUsers((res) => {
            for(var id = 0; id < res.length; id++){
                res[id].ranking = id + 1;
            }
            res.filter((v, i, a) => v.showOnRaking)
            console.log(res)
            this.setState({ users: res, rankLoading: false })
        }, 100, 0)
    }

    render(){   
        return(
            <div>
                <Navbar/>
                <div className="ranking">
                    <h3>Ranking</h3>
                    {this.state.rankLoading ? (
                        <div> Loading... </div>
                    ):(
                        <table>
                            <thead>
                                <tr>
                                    <th className="text-center">Rank</th>
                                    <th className="text-center">User</th>
                                    <th className="text-center">YOB</th>
                                    <th className="text-center">Rating</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.users.map(u => (
                                    <tr key={u.id}>
                                        <td className="text-center">
                                            {u.ranking}
                                        </td>
                                        <td className="text-center" style={{display: 'flex', flexWrap: 'wrap'}}>
                                            <a className={Init.colors[u.color]} style={{float: 'left'}} href={'user/' + u.username}>{u.username}</a>
                                            <p style={{float: 'right'}}>{u.school}</p>
                                        </td>
                                        <td className="text-center">
                                            {u.yob}
                                        </td>
                                        <td className="text-center">
                                            <p className={Init.colors[u.color]}>{u.rating}</p>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    )}
                </div>
            </div>
        )
    }
}

module.exports = App