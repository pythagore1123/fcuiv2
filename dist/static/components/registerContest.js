import React, { Component } from 'react'
import { FreeContest as FC, history, toast } from '../js/action'
import Navbar from '../components/navbar'
import Load from '../components/loader'


class App extends Component {

    constructor(props){
        super(props)

        this.state = {
            user: null,
            contest: null
        }

        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentDidMount(){
        document.title = 'Đăng kí contest'
        let cid = this.props.match.params.cid

        FC.getSession((res) => {
            if(!res.logined){
                toast.error('Thông báo', 'Bạn phải đăng nhập trước')
                history.push({ pathname: '/login' })
            }
            else{
                this.setState({ user: res })
            }
        })

        FC.getSingleContest({cid: cid}, (res) => {
            console.log(res)
            if(res.err){
                toast.error('Thông báo', 'Đã có lỗi xảy ra, vui lòng báo cho tình nguyện viên để được khắc phục')
                history.push({ pathname: '/home' })
            }
            
            if(res.hasRegistered){
                toast.error('Thông báo', 'Bạn đã đăng ký contest này rồi')
                history.push({ pathname: '/home' })
            }
            
            this.setState({ contest: res })

            if(res.IsDuring) { document.getElementsByTagName('input')[0].disabled = true }
        })
    }

    handleSubmit(e){
        e.preventDefault()
        let target = e.target,
            displayOnRanking = target[0].value == 'on' ? true : false,
            cid = this.props.match.params.cid,
            params = {
                cid: cid,
                displayOnRanking: displayOnRanking
            }

            FC.regSingleContest(params, (res) => {
                if(res.err){
                    toast.error('Thông báo', 'Đã có lỗi xảy ra, vui lòng báo cho tình nguyện viên để được khắc phục')
                    history.push({ pathname: '/contest' })
                }

                toast.success('Thông báo', 'Đăng kí thành công')
                history.push({ pathname: '/contest' })
            })
    }

    render(){
        let contest = this.state.contest

        return(
            <div>
                <Navbar/>
                <div className="reg-contest">
                    {contest == null ? (
                        <div className="center" style={{width: '300px', height: '300px'}}>
                            <Load/>
                        </div>
                    ):(
                        <div className="content">
                            <legend><h3>{'Đăng kí tham gia ' + contest.name }</h3></legend>
                            <div className="rules">
                                <p> Hãy chắc chắn bạn đã đọc nội quy </p>
                                <ol>
                                    <li>Không được trao đổi với thí sinh khác.</li>
                                    <li>Không sử dụng code, trình sinh test không phải do mình làm ra.</li>
                                    <li>Không chia sẻ, gợi lý lời giải trong lúc khì thi đang diễn ra.</li>
                                    <li>Không được sử dụng nhiều tài khoản tham gia cùng lúc</li>
                                    <li>Không cố khai thác lỗi, tấn công hệ thống trong quá trình diễn ra kì thi</li>
                                    <li>Nếu muốn khai thác lỗi, mọi ý kiến đóng góp hãy liên hệ với đội TNV Free<b>Contest</b> qua fanpage trên <a href="https://www.facebook.com/kc97blf">Facebook</a></li>
                                </ol>
                            </div>
                            {contest.code == 1 ? (
                                <center>
                                    <strong>Lưu ý!</strong> Mọi thí sinh đăng ký trực tiếp trên trang web sẽ đều bị ẩn khỏi bảng xếp hạng. Mọi thắc mắc liên hệ Fanpage của Free Contest trên Facebook.
                                </center>
                            ):(
                                <div/>
                            )}
                            {contest.code == 0 && contest.IsDuring ? (
                                <center>
                                    <strong>Lưu ý!</strong> Kỳ thi đang diễn ra, vì vậy bạn sẽ bị ẩn khỏi bảng xếp hạng.
                                </center>
                            ):(
                                <div/>
                            )}
                            <form onSubmit={(e) => this.handleSubmit(e)}>
                                <center>
                                    <div>
                                        <input defaultChecked={contest.code == 1 ? false : !contest.IsDuring} type="checkbox"/>
                                        <label>Hiện trên bảng xếp hạng</label>
                                    </div>
                                    <button type="submit" className="btn btn-primary">Tôi đã hiểu và đăng kí</button>
                                </center>
                            </form>
                        </div>
                    )}  
                </div>
            </div>
        )
    }
}

module.exports = App