import React, { Component } from 'react'
import moment from 'moment'
import Navbar from '../components/navbar'
import ReactTooltip from 'react-tooltip'
import { FreeContest as FC } from '../js/action'
import Init from '../js/init'

class App extends Component{

    constructor(props){
        super(props)
        this.state={
            contests: {
                present: [],
                upcoming: [],
                recent: [],
                registered: []
            },
            logined: false,
            user: null
        }
    }

    componentWillMount(){
        FC.getSession((res) => {
            this.setState({ user: res, logined: res.logined })
        })
    }

    componentDidMount(){
        moment.locale('vi')

        document.title="Contest"

        FC.getContest((contests) => {
            console.log(contests)
            let res, recent = [], upcoming = [], present = [], registered = []
                contests.forEach((c) => {
                    let startedTime = (new Date(c.startTime)).getTime();
                    let endTime = (new Date(c.endTime)).getTime();
                    let currentTime = new Date().getTime();

                    if (endTime < currentTime) {
                        recent.push(c);
                    } else
                    if (startedTime > currentTime) {
                        upcoming.push(c);
                    } else
                    if (startedTime <= currentTime && currentTime <= endTime) {
                        present.push(c);
                    };
                    
                    if(c.isRegistered) registered.push(c)
                })

                // recent[0].isRegistered = true
                // recent[1].isRegistered = true
                // upcoming.push(recent[0])
                // present.push(recent[1])

                res = {recent: recent, upcoming: upcoming, present: present}

                res.recent.reverse()
                console.log(res)

            res = {present: present, upcoming: upcoming, recent: recent, registered: registered.reverse()}
            this.state.contests = res
            this.setState({ contests: res })
        }, 10)
        
    }

    render(){
        return(
            <div>
                <Navbar/>    
                <div className="contest-page row">
                    <div className="col-10 offset-1">
                        <div className="present">
                            <h3>Contest đang diễn ra</h3>
                            <table>
                                <thead>
                                    <tr>
                                        <th className="text-center">Bắt đầu</th>
                                        <th className="text-center">Tên Contest</th>
                                        <th className="text-center">Thời lượng</th>
                                        <th className="text-center">Người ra đề</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.contests.present.map(c => (
                                        <tr key={c.id}>
                                            <td className="text-center">
                                                {/* <a data-tip={moment(c.startTime).from(moment())} href={'https://www.timeanddate.com/worldclock/fixedtime.html?iso=' + c.startTime}>{moment(c.startTime).format('llll')}</a> */}
                                                <a data-tip={moment(c.startTime).from(moment())} href={'https://www.timeanddate.com/worldclock/fixedtime.html?iso=' + c.startTime}>{moment(c.startTime).from(moment())}</a>
                                            </td>
                                            <td className="text-center">
                                                <a href={c.facebook_url == '' ? 'javascript:void(0)' : c.facebook_url}>{c.name}</a>
                                            </td>
                                            <td className="text-center">
                                                {c.duration} tiếng
                                            </td>
                                            <td className="text-center">
                                                {c.wrrts.map(w => (
                                                    <div><a className={Init.colors[w.color]} href={'user/' + w.username}>{w.username}</a></div>
                                                ))}
                                            </td>
                                            <td className="text-center">
                                                <a href={'autologin/' + c.id} style={{display: c.isRegistered ? 'block' : 'none'}}>Tham gia</a>
                                                <a href={'register/new/' + c.id} style={{display: !c.isRegistered ? 'block' : 'none'}}>Đăng kí</a>
                                                <a href={'https://freecontest.xyz/admin/edit/contest/' + c.id} style={{display: (this.state.logined && this.state.user.role == 'admin') ? 'block' : 'none'}}>Sửa</a>
                                            </td>
                                            <td className="text-center">
                                                <div><a href="https://code.freecontest.xyz/ranking" target="_blank">Ranking</a></div>
                                                <div>
                                                    <a href={'registered/' + c.id}>
                                                        <strong><i className="fas fa-users"></i> x{c.cntReg}</strong>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    ))}
                                    <tr style={{display: this.state.contests.present.length > 0 ? 'none' : ''}}><td className="text-center" colSpan="6"><i>Không có contest nào đang diễn ra</i></td></tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="upcoming">
                            <h3>Contest sắp tới</h3>
                            <table>
                                <thead>
                                    <tr>
                                        <th className="text-center">Bắt đầu</th>
                                        <th className="text-center">Tên Contest</th>
                                        <th className="text-center">Thời lượng</th>
                                        <th className="text-center">Người ra đề</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.contests.upcoming.map(c => (
                                        <tr key={c.id}>
                                            <td className="text-center">
                                                {/* <a data-tip={moment(c.startTime).from(moment())} href={'https://www.timeanddate.com/worldclock/fixedtime.html?iso=' + c.startTime}>{moment(c.startTime).format('llll')}</a> */}
                                                <a data-tip={moment(c.startTime).from(moment())} href={'https://www.timeanddate.com/worldclock/fixedtime.html?iso=' + c.startTime}>{moment(c.startTime).from(moment())}</a>
                                            </td>
                                            <td className="text-center">
                                                <a href={c.facebook_url == '' ? 'javascript:void(0)' : c.facebook_url}>{c.name}</a>
                                            </td>
                                            <td className="text-center">
                                                {c.duration} tiếng
                                            </td>
                                            <td className="text-center">
                                                {c.wrrts.map(w => (
                                                    <div><a className={Init.colors[w.color]} href={'user/' + w.username}>{w.username}</a></div>
                                                ))}
                                            </td>
                                            <td className="text-center">
                                                <div>
                                                    <a href={'/register/new/' + c.id} style={{display: c.isRegistered ? 'none' : 'block'}}> Đăng kí </a>
                                                    <span style={{color: 'rgb(50, 195, 50)', fontWeight: 'bold', display: !c.isRegistered ? 'none' : 'block'}}> Đã đăng kí </span>
                                                </div>
                                                <div style={{display: (this.state.logined && this.state.user.role == 'admin') ? 'block' : 'none'}}>
                                                    <a href={'https://freecontest.xyz/admin/edit/contest/' + c.id}> Sửa </a>
                                                </div>
                                            </td>
                                            <td className="text-center">
                                                <div>
                                                    <a href={'registered/' + c.id}>
                                                        <strong><i className="fas fa-users"></i> x{c.cntReg}</strong>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    ))}
                                    <tr style={{display: this.state.contests.upcoming.length > 0 ? 'none' : ''}}><td className="text-center" colSpan="6">Không có contest nào sắp tới</td></tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="recent">
                            <h3>Contest đã qua</h3>
                            <table>
                                <thead>
                                    <tr>
                                        <th className="text-center">Bắt đầu</th>
                                        <th className="text-center">Tên Contest</th>
                                        <th className="text-center">Thời lượng</th>
                                        <th className="text-center">Người ra đề</th>
                                        <th className="text-center">Luyện tập</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.contests.recent.map(c => (
                                        <tr key={c.id}>
                                            <td className="text-center">
                                                {/* <a data-tip={moment(c.startTime).from(moment())} href={'https://www.timeanddate.com/worldclock/fixedtime.html?iso=' + c.startTime}>{moment(c.startTime).format('llll')}</a> */}
                                                <a data-tip={moment(c.startTime).from(moment())} href={'https://www.timeanddate.com/worldclock/fixedtime.html?iso=' + c.startTime}>{moment(c.startTime).from(moment())}</a>
                                            </td>
                                            <td className="text-center">
                                                <a href={c.facebook_url == '' ? 'javascript:void(0)' : c.facebook_url}>{c.name}</a>
                                            </td>
                                            <td className="text-center">
                                                {c.duration} tiếng
                                            </td>
                                            <td className="text-center">
                                                {/* {console.log(c.id, c.wrrts)} */}
                                                {c.wrrts.map(w => (
                                                    <div><a className={Init.colors[w.color]} href={'user/' + w.username}>{w.username}</a></div>
                                                ))}
                                            </td>
                                            <td className="text-center">
                                                <div>
                                                    {c.practice_url == null ? (
                                                        <strong>
                                                            <i className="fas fa-times"></i>
                                                        </strong>
                                                    ):(
                                                        <a href={c.practice_url}>Luyện tập</a>
                                                    )}
                                                </div>
                                            </td>
                                        </tr>
                                    ))}
                                    <tr style={{display: this.state.contests.recent.length > 0 ? 'none' : ''}}><td className="text-center" colSpan="5">Không có contest nào đã qua</td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <ReactTooltip place="left"/>
            </div>
        )
    }
}


module.exports = App