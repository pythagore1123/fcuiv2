import React, { Component } from 'react'
import { FreeContest as FC, history, toast } from '../js/action'
import Navbar from '../components/navbar'
import Loader from '../components/loader'

class App extends Component{

    constructor(props){
        super(props)
        this.state = {
            user: null,
            logined: false,
        }

        this.checkContest = this.checkContest.bind(this)
        this.prepare = this.prepare.bind(this)
        this.autoLogin = this.autoLogin.bind(this)
    }

    componentDidMount(){
        document.title = "Tham gia kì thi"

        FC.getSession((res) => {
            if(res.logined){
                this.setState({ user: res, logined: res.logined })
                document.getElementById('user').classList.add('checked')
                this.checkContest()
            }
            else{
                toast.error('Thông báo', 'Bạn cần đăng nhập trước')
                history.push({ pathname: '/login' })
            }
        })
    }

    checkContest(){
        let cid = this.props.match.params.cid
        FC.getContestWID(cid, (res) => {
            if(res.length == 0 || res.err != undefined){
                toast.error('Thông báo', 'Không tìm thấy contest với id ' + cid)
                history.push({ pathname: '/contest' })
            }
            else{
                let contest = res[0]

				let startTime = new Date(contest.startTime)
				let endTime = new Date(contest.endTime)
                let current = new Date()
                
                if(startTime <= current && current <= endTime){
                    document.getElementById('contest').classList.add('checked')
                    this.prepare()
                }
                else{
                    toast.error('Thông báo', res.name + ' chưa diễn ra')
                    history.push({ pathname: '/contest' })
                }
            }
        })
    }

    prepare(){
        let cid = this.props.cid,
            uid = this.state.user.id == null ? this.state.user.uid : this.state.user.id
        
        FC.getTmpPassword(uid, cid, (res) => {
            if(res.success == undefined || res.success == null){
                toast.error('Thông báo', 'Không lấy được mật khẩu, hãy liên hệ với FreeContest để được tư vấn')
                history.push({ pathname: '/contest' })
            }
            else{
                let username = this.state.user.username,
                    password = res.tmpPassword

                
                document.getElementById('prepare').classList.add('checked')
                this.autoLogin(username, password)
            }
        })
    }

    async autoLogin(username, password){
        try{
            let cms = new FreeContest_CMS(FC_CMS_URL)
            let res = await cms.login(username, password)

            if(res){
                window.location.replace(FC_CMS_URL)
            }
            else{
                toast.error('Thông báo', 'Có lỗi khi đăng nhập')
                history.push({ pathname: '/contest' })
            }
        }
        catch(e){
            console.log(e)
            toast.error('Thông báo', 'Có lỗi khi đăng nhập')
            history.push({ pathname: '/contest' })
        }
    }

    render(){
        return(
            <div>
                <Navbar/>
                <center className="autologin-main">
                    <div className="load">
                        <Loader/>
                    </div>
                    <div className="todos">
                        <ol>
                            <div id="user" className="todo">Kiểm tra người dùng đã đăng nhập</div>
                            <div id="contest" className="todo">Kiểm tra tính hợp lệ của kì thi</div>
                            <div id="prepare" className="todo">Chuẩn bị mật khẩu để đăng nhập vào hệ thống CMS</div>
                        </ol>
                    </div>
                </center>
            </div>
        )
    }
}

// Created by Tuan Kiet Ho (tuankiet65) of Free Contest Server Team

const FC_CMS_URL = "https://code.freecontest.xyz/"

class FreeContest_CMS {
    constructor (cms_url) {
        this.cms_url = cms_url
    }
    
    async get_xsrf_token() {
        let response = await fetch(`${this.cms_url}`, {
            method: "GET",
            credentials: "include"
        })
        
        let text = await response.text()
        let parser = new DOMParser()
        let doc = parser.parseFromString(text, "text/html")
        let xsrf_token = doc.getElementsByName("_xsrf")[0].value
        
        return xsrf_token;
    }
    
    async logout() {
        let xsrf_token = await this.get_xsrf_token()
        
        let params = new URLSearchParams()
        params.append("_xsrf", xsrf_token)
        
        let response = await fetch(`${this.cms_url}/logout`, {
            method: "POST",
            body: params,
            credentials: "include"
        })
        
        return true;
    }
    
    async login(username, password) {
        let xsrf_token = await this.get_xsrf_token()
        
        let params = new URLSearchParams()
        params.append("_xsrf", xsrf_token)
        params.append("username", username)
        params.append("password", password)
        
        let response = await fetch(`${this.cms_url}/login`, {
            method: "POST",
            body: params,
            credentials: "include"
        })
        
        let final_url = response.url
        if (final_url.includes("?login_error=true")) return false;
        return true;
    }    
}

async function cms_logout() {
    let cms = new FreeContest_CMS(FC_CMS_URL);
    let result = await cms.logout();
    
    window.location.replace(FC_CMS_URL);
}

module.exports = App