import React, { Component } from 'react'
import { userInfo } from 'os';


class Home extends Component{
    constructor(props){
        super(props)
        this.state = {
            posts: null
        }
    }

    componentDidMount(){
        //get posts
    }

    render() {
        const { posts } = this.state
        return (
            <div>
                <div class="col-sm-8">
                    {(posts && !PostLoading) ? (posts.map(post => (
                        <div class="panel panel-default" visibility={post.show ? 'visible' : 'hidden'}>
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <span style="color: navy;">{ post.title }</span>
                                    <small uib-tooltip={ post.createdAt } tooltip-placement="right">Đăng: { moment(post.createdAt).from(moment()) } bởi { post.author } / </small> <small uib-tooltip={ post.updatedAt } tooltip-placement="right">Cập nhật lần cuối: { moment(post.updatedAt).from(moment()) }</small>
                                    <div visibility={(logined && user.role == 'admin') ? 'visible' : 'hidden'} class="btn-group pull-right" is-open="post.isopen">
                                        <button id="single-button" type="button" class="btn btn-xs btn-default" uib-dropdown-toggle ng-disabled="disabled">
                                        E <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" uib-dropdown-menu role="menu" aria-labelledby="single-button">
                                            <li role="menuitem"><a href="admin/edit/post/{{ post.id }}">Sửa bài viết</a></li>
                                            <li role="menuitem"><a href="javascript:void(0);" ng-click="delete(post)">Xóa bài viết</a></li>
                                        </ul>
                                    </div>
                                </h3>
                            </div>

                            <Link class="panel-body" visibility={(!post.formLoading) ? "visible": "hidden"} to={post.HTMLContent}></Link>
                            <div class="panel-body" visibility={post.formLoading ? "visible" : "hidden"}>
                                <div class="progress">
                                    <div class="progress-bar active progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%"><i>Loading...</i></div>
                                </div>
                            </div>

                            <div class="panel-footer post-footer">
                                <a href={`post/${post.id}`}>
                                    <i class="fa fa-comments-o" aria-hidden="true"></i> <strong> ({ post.cntComment }) Bình luận</strong>
                                </a>
                                <div class="pull-right">
                                    <a class="share-button" target="_blank" href={ post.fbShareLink } style="background: url(./static/img/share/facebook.png)"></a>
                                </div>
                            </div>
                        </div>
                    ))) : (<Loading/>)}
                </div>
                
            </div>
        )
    }
}

const Loading = (
    <div>
        <div class="progress">
            <div class="progress-bar active progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%"><i>Loading...</i></div>
        </div>
    </div>
)