const Init = {
    colors: ["dark-red", "black", "gray", "green", "cyan", "blue", "violet", "orange", "orange", "red", "red", "super-red", "user"],
    titles: ["Headquarters", "Unrated", "Newbie", "Pupil", "Specialist", "Expert", "Candidate Master", "Master", "International Master", "Grandmaster", "International Grandmaster", "Legendary Grandmaster", "User"],
}

module.exports = Init